﻿#include <stdio.h>
#include <conio.h>
#define _USE_MATH_DEFINES // for C
#include <math.h>

double G(float a, float x) {
	float o2 = (27 * a * a + 33 * a * x + 10 * x * x);
	if (o2 == 0) return 0;
	return -8 * (7 * a * a + 34 * a * x - 5 * x * x) / o2;
}

double F(double a, double x) {
	float o2 = sin(72.0 * a * a - 5.0 * a * x - 12.0 * x * x - M_PI_2);
	if (o2 == 0) return 0;
	return -1 / o2;
}

double Y(double a, double x) {
	float o2 = 42 * a * a + 53 * a * x + 15 * x * x + 1;
	if (o2 < 0) return 0;
	return log(o2);
}

int main(void)
{
	float a, g, f, y;
	float xMin, xMax;
	int xCount;

	printf("Vvedite xMin & xMax: ");
	scanf_s("%f%f", &xMin, &xMax);

	printf("Vvedite colichestvo shagov: ");
	scanf_s("%i", &xCount);
	if (xCount <= 0) {
		printf("Oshibochniy vvod");
		return -1;
	}

	printf("Vvedite a: ");
	scanf_s("%f", &a);

	printf("Vvedite N funkcii (1-3): ");
	int n;
	scanf_s("%i", &n);
	if (n < 1 || n > 3) {
		printf("Oshibochniy vvod");
		return -1;
	}

	float x = xMin;
	float xDelta = (xMax - xMin) / xCount;
	printf("Nazhmite ESC dlya vihoda iz cikla...\n");
	for (int i = 0; i < xCount; ++i) {
		switch (n) {
		case 1:
			g = G(a, x);
			printf("x=%f g=%f\n", x, g);
			break;
		case 2:
			f = F(a, x);
			printf("x=%f f=%f\n", x, f);
			break;
		case 3:
			y = Y(a, x);
			printf("x=%f y=%f\n", x, y);
			break;
		}
		x += xDelta;
		if (_kbhit()) {
			int keyCode = _getch();
			if (keyCode == 27)
				break;
		}
	}
}
