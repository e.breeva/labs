﻿#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <conio.h>
#include <float.h>
#define _USE_MATH_DEFINES // for C


#include <math.h>
#include <string>

double G(double a, double x) {
	double o2 = (27 * a * a + 33 * a * x + 10 * x * x);
	if (o2 == 0) return 0;
	return -8 * (7 * a * a + 34 * a * x - 5 * x * x) / o2;
}

double F(double a, double x) {
	double o2 = sin(72.0 * a * a - 5.0 * a * x - 12.0 * x * x - M_PI_2);
	if (o2 == 0) return 0;
	return -1 / o2;
}

double Y(double a, double x) {
	double o2 = 42 * a * a + 53 * a * x + 15 * x * x + 1;
	if (o2 < 0) return 0;
	return log(o2);
}

struct Result {
	double g;
	double f;
	double y;
};

int save(const char* filename, Result* st, int n)
{
	FILE* fp = fopen(filename, "wb");
	if (fp == NULL)
	{
		perror("Oshibka sozdania faila");
		return -1;
	}
	fwrite(&n, sizeof(int), 1, fp);
	fwrite(st, sizeof(Result), n, fp);
	fclose(fp);
	return 0;
}

Result* load(const char* filename, int& n)
{
	FILE* fp = fopen(filename, "rb");
	if (fp == NULL)
	{
		perror("Oshibka chteniya faila");
		return NULL;
	}
	fread(&n, sizeof(int), 1, fp);

	if (n <= 0) return NULL;
	Result* result = new Result[n];
	fread(result, sizeof(Result), n, fp);

	fclose(fp);
	return result;
}

int main(void)
{
	const char* filename = "data.txt";
	char sxMin[10] = "\0", sxMax[10] = "\0";
	char saMin[10] = "\0", saMax[10] = "\0";
	char sCount[10] = "\0";

	double xMin, xMax;
	double aMin, aMax;
	int xCount;

	printf("Vvedite X (min & max): ");
	scanf_s("%9s%9s", sxMin, sizeof(sxMin), sxMax, sizeof(sxMax));
	xMin = atof(sxMin);
	xMax = atof(sxMax);

	printf("Vvedite A (min & max): ");
	scanf_s("%9s%9s", saMin, sizeof(saMin), saMax, sizeof(saMax));
	aMin = atof(saMin);
	aMax = atof(saMax);

	printf("Vvedite colichestvo shagov: ");
	scanf_s("%9s", sCount, sizeof(sCount));
	xCount = atoi(sCount);
	if (xCount <= 0) {
		perror("Oshibochniy vvod");
		return -1;
	}

	double xDelta = (xMax - xMin) / xCount;
	double aDelta = (aMax - aMin) / xCount;
	printf("Nazhmite ESC dlya vihoda iz cikla...\n");
	bool breakNow = false;

	Result* result = new Result[xCount * xCount];
	double x = xMin;
	for (int i = 0; i < xCount; ++i) {
		double a = aMin;
		for (int j = 0; j < xCount; j++) {
			result[i * xCount + j].g = G(a, x);
			result[i * xCount + j].f = F(a, x);
			result[i * xCount + j].y = Y(a, x);

			if (_kbhit()) {
				int keyCode = _getch();
				if (keyCode == 27) {
					breakNow = true;
					break;
				}
			}
			a += aDelta;
		}
		x += xDelta;
		if (breakNow) break;
	}

	printf("\nVicheslenniy result:\n");
	for (int i = 0; i < xCount * xCount; ++i) {
		printf("G=%f, F=%f, Y=%f\n", result[i].g, result[i].f, result[i].y);
	}
	save(filename, result, xCount * xCount);
	delete[] result;

	int n = 0;
	result = load(filename, n);
	if (n > 0) {
		printf("\nZagruzhenniy result (kol-vo: %i):\n", n);
		for (int i = 0; i < n; ++i) {
			printf("G=%f, F=%f, Y=%f\n", result[i].g, result[i].f, result[i].y);
		}
		delete[] result;
	}
	else {
		perror("Net dannih");
	}

}