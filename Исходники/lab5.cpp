﻿#include <stdio.h>
#include <conio.h>
#include <float.h>
#define _USE_MATH_DEFINES // for C
#include <math.h>
#include <string>

double G(double a, double x) {
	double o2 = (27 * a * a + 33 * a * x + 10 * x * x);
	if (o2 == 0) return 0;
	return -8 * (7 * a * a + 34 * a * x - 5 * x * x) / o2;
}

double F(double a, double x) {
	double o2 = sin(72.0 * a * a - 5.0 * a * x - 12.0 * x * x - M_PI_2);
	if (o2 == 0) return 0;
	return -1 / o2;
}

double Y(double a, double x) {
	double o2 = 42 * a * a + 53 * a * x + 15 * x * x + 1;
	if (o2 < 0) return 0;
	return log(o2);
}

int substrcount(std::string src, std::string sub) {
	int start = 0;
	int count = 0;
	int pos = 0;
	for (;;) {
		pos = src.find(sub.c_str(), start);
		if (pos != -1) {
			start = pos + sub.size();
			count++;
		}
		else {
			break;
		}
	}
	return count;
}

int main(void)
{
	char sxMin[10], sxMax[10];
	char saMin[10], saMax[10];
	char sCount[10];

	float xMin, xMax;
	float aMin, aMax;
	int xCount;

	printf("Vvedite X (min & max): ");
	scanf_s("%9s%9s", sxMin, sizeof(sxMin), sxMax, sizeof(sxMax));
	xMin = atof(sxMin);
	xMax = atof(sxMax);

	printf("Vvedite A (min & max): ");
	scanf_s("%9s%9s", saMin, sizeof(saMin), saMax, sizeof(saMax));
	aMin = atof(saMin);
	aMax = atof(saMax);

	printf("Vvedite colichestvo shagov: ");
	scanf_s("%9s", sCount, sizeof(sCount));
	xCount = atoi(sCount);
	if (xCount <= 0) {
		printf("Oshibochniy vvod");
		return -1;
	}


	float xDelta = (xMax - xMin) / xCount;
	float aDelta = (aMax - aMin) / xCount;
	printf("Nazhmite ESC dlya vihoda iz cikla...\n");
	bool breakNow = false;

	double** g = new double* [xCount];
	double** f = new double* [xCount];
	double** y = new double* [xCount];

	int i = 0;
	int j = 0;
	std::string result = "";

	for (double x = xMin; x <= xMax; x += xDelta, i++) {
		g[i] = new double[xCount];
		f[i] = new double[xCount];
		y[i] = new double[xCount];
		j = 0;
		for (double a = aMin; a <= aMax; a += aDelta, j++) {
			g[i][j] = G(a, x);	
			f[i][j] = F(a, x);			
			y[i][j] = Y(a, x);
			result += std::to_string(g[i][j])+ std::to_string(f[i][j])+ std::to_string(y[i][j]);
			if (_kbhit()) {
				int keyCode = _getch();
				if (keyCode == 27) {
					breakNow = true;
					break;
				}
			}
		}
		if (breakNow) break;
	}


	printf("Result: %s\n\n", result.c_str());

	for (i = 0; i < xCount; ++i) {
		delete[] g[i];
		delete[] f[i];
		delete[] y[i];
	}

	char t[20];
	printf("Vvedite shablon dlya poiska: ");
	scanf_s("%19s", t, sizeof(t));
	int pos = result.find(t);
	if (pos >= 0) {
		printf("Shablon naiden v posicii: %i\n", pos);
		int cnt = substrcount(result, std::string(t));
		printf("Kol-vo vhozhdebiy: %i\n", cnt);
	}
	else {
		printf("Shablon ne naiden");
	}
}