﻿#include <stdio.h>
#define _USE_MATH_DEFINES // for C
#include <math.h>

double G(float a, float x) {
	return -8 * (7 * a * a + 34 * a * x - 5 * x * x) / (27 * a * a + 33 * a * x + 10 * x * x);
}

double F(double a, double x) {
	return -1 / sin(72.0 * a * a - 5.0 * a * x - 12.0 * x * x - M_PI_2);
}

double Y(double a, double x) {
	return log(42 * a * a + 53 * a * x + 15 * x * x + 1);
}

int main(void)
{
	float a, x, g, f, y;

	printf("Vvedite x: ");
	scanf_s("%f", &x);
	if (x < 0) {
		printf("Znachenie X < 0. Oshibka");
		return 0;
	}
	printf("Vvedite a: ");
	scanf_s("%f", &a);


	printf("Vvedite N funkcii (1-3): ");
	int n;
	scanf_s("%i", &n);

	switch (n) {
		case 1:
			g = G(a, x);
			printf("g=%f\n\n", g);
			break;
		case 2:
			f = F(a, x);
			printf("f=%f\n\n", f);
			break;
		case 3:
			y = Y(a, x);
			printf("y=%f\n\n", y);
			break;
		default:
			printf("Oshibochniy vvod");
		}
}
