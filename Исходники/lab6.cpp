﻿#include <stdio.h>
#include <conio.h>
#include <float.h>
#define _USE_MATH_DEFINES // for C
#include <math.h>
#include <string>

double G(double a, double x) {
	double o2 = (27 * a * a + 33 * a * x + 10 * x * x);
	if (o2 == 0) return 0;
	return -8 * (7 * a * a + 34 * a * x - 5 * x * x) / o2;
}

double F(double a, double x) {
	double o2 = sin(72.0 * a * a - 5.0 * a * x - 12.0 * x * x - M_PI_2);
	if (o2 == 0) return 0;
	return -1 / o2;
}

double Y(double a, double x) {
	double o2 = 42 * a * a + 53 * a * x + 15 * x * x + 1;
	if (o2 < 0) return 0;
	return log(o2);
}

struct Result {
	double g;
	double f;
	double y;
};

int main(void)
{
	char sxMin[10], sxMax[10];
	char saMin[10], saMax[10];
	char sCount[10];

	double xMin, xMax;
	double aMin, aMax;
	int xCount;

	printf("Vvedite X (min & max): ");
	scanf_s("%9s%9s", sxMin, sizeof(sxMin), sxMax, sizeof(sxMax));
	xMin = atof(sxMin);
	xMax = atof(sxMax);

	printf("Vvedite A (min & max): ");
	scanf_s("%9s%9s", saMin, sizeof(saMin), saMax, sizeof(saMax));
	aMin = atof(saMin);
	aMax = atof(saMax);

	printf("Vvedite colichestvo shagov: ");
	scanf_s("%9s", sCount, sizeof(sCount));
	xCount = atoi(sCount);
	if (xCount <= 0) {
		printf("Oshibochniy vvod");
		return -1;
	}


	float xDelta = (xMax - xMin) / xCount;
	float aDelta = (aMax - aMin) / xCount;
	printf("Nazhmite ESC dlya vihoda iz cikla...\n");
	bool breakNow = false;

	Result* result = new Result[xCount * xCount];
	int i = 0;
	for (double x = xMin; x <= xMax; x += xDelta) {
		for (double a = aMin; a <= aMax; a += aDelta, i++) {
			result[i].g = G(a, x);
			result[i].f = F(a, x);
			result[i].y = Y(a, x);

			if (_kbhit()) {
				int keyCode = _getch();
				if (keyCode == 27) {
					breakNow = true;
					break;
				}
			}
		}
		if (breakNow) break;
	}
	for (i = 0; i < xCount * xCount; ++i) {
		printf("G=%f, F=%f, Y=%f\n", result[i].g, result[i].f, result[i].y);			
	}
	delete[] result;
}