﻿#include <stdio.h>
#include <conio.h>
#include <float.h>
#define _USE_MATH_DEFINES // for C
#include <math.h>

double G(double a, double x) {
	double o2 = (27 * a * a + 33 * a * x + 10 * x * x);
	if (o2 == 0) return 0;
	return -8 * (7 * a * a + 34 * a * x - 5 * x * x) / o2;
}

double F(double a, double x) {
	double o2 = sin(72.0 * a * a - 5.0 * a * x - 12.0 * x * x - M_PI_2);
	if (o2 == 0) return 0;
	return -1 / o2;
}

double Y(double a, double x) {
	double o2 = 42 * a * a + 53 * a * x + 15 * x * x + 1;
	if (o2 < 0) return 0;
	return log(o2);
}

int main(void)
{
	float xMin, xMax;
	float aMin, aMax;
	int xCount;

	printf("Vvedite X (min & max): ");
	scanf_s("%f%f", &xMin, &xMax);

	printf("Vvedite A (min & max): ");
	scanf_s("%f%f", &aMin, &aMax);

	printf("Vvedite colichestvo shagov: ");
	scanf_s("%i", &xCount);
	if (xCount <= 0) {
		printf("Oshibochniy vvod");
		return -1;
	}


	float xDelta = (xMax - xMin) / xCount;
	float aDelta = (aMax - aMin) / xCount;
	printf("Nazhmite ESC dlya vihoda iz cikla...\n");
	bool breakNow = false;

	double** g = new double* [xCount];
	double** f = new double* [xCount];
	double** y = new double* [xCount];

	double minG = DBL_MAX;
	double minF = DBL_MAX;
	double minY = DBL_MAX;

	double maxG = -DBL_MAX;
	double maxF = -DBL_MAX;
	double maxY = -DBL_MAX;

	int i = 0;
	int j = 0;
	for (double x = xMin; x <= xMax; x += xDelta, i++) {
		g[i] = new double[xCount];
		f[i] = new double[xCount];
		y[i] = new double[xCount];
		j = 0;
		for (double a = aMin; a <= aMax; a += aDelta, j++) {
			g[i][j] = G(a, x);
			if (g[i][j] < minG) minG = g[i][j];
			if (g[i][j] > maxG) maxG = g[i][j];

			f[i][j] = F(a, x);
			if (f[i][j] < minF) minF = f[i][j];
			if (f[i][j] > maxF) maxF = f[i][j];

			y[i][j] = Y(a, x);
			if (y[i][j] < minY) minY = y[i][j];
			if (y[i][j] > maxY) maxY = y[i][j];

			if (_kbhit()) {
				int keyCode = _getch();
				if (keyCode == 27) {
					breakNow = true;
					break;
				}
			}
		}
		if (breakNow) break;
	}


	printf("Min/Max G: %f / %f\n", minG, maxG);
	printf("Min/Max F: %f / %f\n", minF, maxY);
	printf("Min/Max Y: %f / %f\n", minY, maxY);

	printf("Znachenie G:\n");
	i = 0;
	for (double x = xMin; x <= xMax; x += xDelta, i++) {
		j = 0;
		for (double a = aMin; a <= aMax; a += aDelta, j++) {
			printf("x=%f, a=%f, g=%f\t", x, a, g[i][j]);
		}
		printf("\n");
	}
	

	printf("Znachenie F:\n");
	i = 0;
	for (double x = xMin; x <= xMax; x += xDelta, i++) {
		j = 0;
		for (double a = aMin; a <= aMax; a += aDelta, j++) {
			printf("x=%f, a=%f, g=%f\t", x, a, f[i][j]);
		}
		printf("\n");
	}
	

	printf("Znachenie Y:\n");
	i = 0;
	for (double x = xMin; x <= xMax; x += xDelta, i++) {
		j = 0;
		for (double a = aMin; a <= aMax; a += aDelta, j++) {
			printf("x=%f, a=%f, g=%f\t", x, a, y[i][j]);
		}
		printf("\n");
	}

	for (i = 0; i < xCount;  ++i) {
		delete[] g[i];
		delete[] f[i];
		delete[] y[i];
	}	
}